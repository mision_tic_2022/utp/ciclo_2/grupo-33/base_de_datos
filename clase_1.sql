
/*
 * Esto es un comentario
 * de varias lineas
 * de código
*/

CREATE TABLE grupo_33(
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	jornada VARCHAR(10) NOT NULL,
	universidad VARCHAR(50) NOT NULL,
	cant_estudiantes INTEGER,
	prom_edad DOUBLE NOT NULL,
	conectado BOOLEAN
);


--Insertar datos en la tabla

INSERT INTO grupo_33(
	id, jornada, universidad,
	cant_estudiantes, prom_edad ,conectado
) 
VALUES(1, "Mañana", "UTP", 20, 25, true);


INSERT INTO grupo_33 (
	id, jornada, universidad, 
	prom_edad, conectado
)
VALUES(2, "Mañana", "UTP", 24, false);

INSERT INTO grupo_33 (
	jornada, universidad, 
	prom_edad, conectado
)
VALUES("Tarde", "UTP", 14, true);


--Consultar registros
SELECT * FROM grupo_33;

SELECT id, universidad, jornada FROM grupo_33;

SELECT id codigo, universidad UNIVERSIDAD FROM grupo_33;


SELECT id AS codigo, universidad AS UNIVERSIDAD FROM grupo_33;

--Modificar un registro
UPDATE grupo_33 SET jornada="Mañana", prom_edad=28 WHERE id=2;

--Eliminar registro
DELETE FROM grupo_33 WHERE id=1;

--Elimnar una tabla
DROP TABLE grupo_33;











